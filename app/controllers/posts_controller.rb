class PostsController < ApplicationController
	before_filter :is_owner, only: [:edit, :destroy]
	
	# before_filter :authenticate_user!, except: [:vote, :show_vote]

	def index
		@posts = Post.all.order("created_at DESC")
	end

	def new
		@post = current_user.posts.build
	end

	def create
		@post = current_user.posts.build(parameters)
		if @post.save
			redirect_to @post
		else
			render 'new'
		end
	end


	def show
		find_post
	end

	def destroy
		find_post
		@post.destroy
		redirect_to root_path
	end

	def update
		find_post
		if @post.update(parameters)
			redirect_to @post
		else
			render 'edit'
		end
	end

	def edit
		find_post
	end

	private
	def parameters
		params.require(:post).permit(:title, :content, :image)
	end

	def find_post
		@post = Post.find(params[:id])
	end

	def is_owner
		find_post
		if @post.user != current_user
			redirect_to @post
		end
	end

end